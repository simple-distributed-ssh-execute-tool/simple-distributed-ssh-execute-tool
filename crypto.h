#ifndef CRYPTO_INCLUDED
#define CRYPTO_INCLUDED

/* ====================================================================
 * Copyright (c) 2002 Johnny Shelley.  All rights reserved.
 *
 * Bcrypt is licensed under the BSD software license. See the file 
 * called 'LICENSE' that you should have received with this software
 * for details
 * ====================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef WIN32	/* These libraries don't exist on Win32 */
#include <unistd.h>
#include <termios.h>
#include <sys/time.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <zlib.h>

typedef unsigned int uInt32;
typedef unsigned long uLong;
/*
 * Author     :  Paul Kocher
 * E-mail     :  pck@netcom.com
 * Date       :  1997
 * Description:  C implementation of the Blowfish algorithm.
 */

#define MAXKEYBYTES 56          /* 448 bits */

typedef struct {
  uInt32 P[16 + 2];
  uInt32 S[4][256];
} BLOWFISH_CTX;

void Blowfish_Init(BLOWFISH_CTX *ctx, unsigned char *key, int keyLen);

void Blowfish_Encrypt(BLOWFISH_CTX *ctx, uInt32 *xl, uInt32
*xr);

void Blowfish_Decrypt(BLOWFISH_CTX *ctx, uInt32 *xl, uInt32
*xr);
/* ====================================================================
 * Copyright (c) 2002 Johnny Shelley.  All rights reserved.
 *
 * Bcrypt is licensed under the BSD software license. See the file 
 * called 'LICENSE' that you should have received with this software
 * for details
 * ====================================================================
 */


/* All options are either 1 for true or 0 for false w/ the exception	*/
/*  of SECUREDELETE which may be anything from 0 to 127.		*/
/* All options may be overridden on the command line.			*/

/* whether or not to compress files */
#define COMPRESS 1
/* send output to stdout */
#define STANDARDOUT 0
/* remove input files after processing */
#define REMOVE 1
/* how many times to overwrite input files before deleting */
#define SECUREDELETE 3
typedef struct _BCoptions {
  unsigned char remove;
  unsigned char standardout;
  unsigned char compression;
  unsigned char type;
  uLong origsize;
  unsigned char securedelete;
} BCoptions;

#define ENCRYPT 0
#define DECRYPT 1

#define endianBig ((unsigned char) 0x45)
#define endianLittle ((unsigned char) 0x54)


#ifdef WIN32 /* Win32 doesn't have random() or lstat */
#define random() rand()
#define initstate(x,y,z) srand(x)
#define lstat(x,y) stat(x,y)
#endif

#ifndef S_ISREG
#define S_ISREG(x) ( ((x)&S_IFMT)==S_IFREG )
#endif

/* ====================================================================
 * Copyright (c) 2002 Johnny Shelley.  All rights reserved.
 *
 * Bcrypt is licensed under the BSD software license. See the file 
 * called 'LICENSE' that you should have received with this software
 * for details
 * ====================================================================
 */


/* from wrapbf.c */
uLong BFEncrypt(char **input, char *key, uLong sz, 
	BCoptions *options);
uLong BFDecrypt(char **input, char *key, char *key2,
	uLong sz, BCoptions *options);

/* from keys.c */
char * getkey(int type);
void mutateKey(char **key, char **key2);

/* from rwfile.c */
int getremain(uLong sz, int dv);
uLong padInput(char **input, uLong sz);
uLong attachKey(char **input, char *key, uLong sz);
uLong readfile(char *infile, char **input, int type, char *key,
	struct stat statbuf);
uLong writefile(char *outfile, char *output, uLong sz, 
	BCoptions options, struct stat statbuf);
int deletefile(char *file, BCoptions options, char *key, struct stat statbuf);

/* from wrapzl.c */
uLong docompress(char **input, uLong sz);
uLong douncompress(char **input, uLong sz, BCoptions options);

/* from main.c */
BCoptions initoptions(BCoptions options);
int usage(char *name);
int memerror();
void set_options(BCoptions *options, int remove, int standardout, int compress);
int assignFiles(char *arg, char **infile, char **outfile, struct stat *statbuf,
        BCoptions *options, char *key);
char* do_crypto(char *file, int remove, int standardout, int compress);

/* from endian.c */
void getEndian(unsigned char **e);
uInt32 swapEndian(uInt32 value);
int testEndian(char *input);
int swapCompressed(char **input, uLong sz);

/* All options are either 1 for true or 0 for false w/ the exception	*/
/*  of SECUREDELETE which may be anything from 0 to 127.		*/
/* All options may be overridden on the command line.			*/

/* whether or not to compress files */
#define COMPRESS 1
/* send output to stdout */
#define STANDARDOUT 0
/* remove input files after processing */
#define REMOVE 1
/* how many times to overwrite input files before deleting */
#define SECUREDELETE 3

#endif
