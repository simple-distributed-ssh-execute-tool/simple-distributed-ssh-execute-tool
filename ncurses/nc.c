#include <ncurses.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>

static WINDOW *create_newwin(int h, int w, int y, int x);
static void destroy_win(WINDOW *local_win);
WINDOW *wins[100];
typedef int (*NCURSES_CALLBACK)(WINDOW *, void *);
int proc_loop(WINDOW* win, void *data);

int main(int argc, char *argv[])
{
        int x, y, w, h, ch;
        int win_num = 4;

        if (argc >= 2) {
                win_num = atoi(argv[1]);
                win_num = win_num ? win_num : 4;
        }


        initscr();
        cbreak();
        noecho();
        keypad(stdscr, TRUE);

        printw("Press F1 to exit");
        refresh();

        int i = 0;
        int min_width = COLS / 2, min_height = LINES / 2;

        int cur_width = 0, cur_height = 0;
        int pid;
        for (i = 0; i < win_num; i++) {
                wins[i] = create_newwin(min_height, min_width, cur_height, cur_width);
                if (cur_width > COLS || (cur_width * 2 + min_width) > COLS) {
                        cur_height += min_height;
                        cur_width = 0;
                } else
                        cur_width += min_width;

        }

        long flag = fcntl(STDIN_FILENO, F_GETFL, 0);
        return fcntl(STDIN_FILENO, F_SETFL, flag|O_NONBLOCK);

        
        if ((pid = fork()) == 0) {
                for (;;) {
                        for (i = 0; i < win_num; i++) {
                                use_window(wins[i], proc_loop, NULL);
                        }
                        sleep(1);
                        if (getch() == KEY_F(1)) {
                                goto exit;
                        }
                }
        }

exit:
        while(wait(NULL) > 0);
        endwin();

        return 0;
}

static WINDOW *create_newwin(int h, int w, int y, int x)
{
        WINDOW *local_win;
        local_win = newwin(h, w, y, x);
        box(local_win, 0, 0);
        refresh();
        wrefresh(local_win);

        return local_win;
}

static void destroy_win(WINDOW *local_win)
{
        wborder(local_win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
        wrefresh(local_win);
        delwin(local_win);
}

int proc_loop(WINDOW* win, void *data)
{
        static n = 0;
        mvwprintw(win, 2, 2, "hello.%d", n++);
        wrefresh(win);
}
