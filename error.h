#ifndef ERROR_INCLUDED
#define ERROR_INCLUDED

#define MAX_MSG_LEN     1024

void err_exit(const char* fmt, ...);


#endif
