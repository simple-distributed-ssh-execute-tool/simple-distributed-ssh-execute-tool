#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include "defines.h"
#include "error.h"

char* mk_string(char *s, const char *str, int size)
{
        if (size <= 0)
                return "";
        if ((s = malloc((size + 1) * sizeof(char))) == NULL)
                err_exit("Could not alloc memory, mk_string()!");
        bzero(s, size + 1);
        if (str)
                s = strcpy(s, str);
        return s;
}

char* split(char *str, char* sep, int field)
{
        char *token;
        char *res;
        int num = 0;

        token = strtok(str, sep);
        if (token == NULL)
                return "";
        
        if (num == field) {
                res = mk_string(res,
                                token,
                                strlen(token));
                return res;
        }

        while (num++ != field && (token = strtok(NULL, sep)) != NULL);

        if (token == NULL)
                return "";

        res = mk_string(res,
                        token,
                        strlen(token));

        return res;
}

char * str_cut_enter(char *str)
{
        int len;
        
        len = strlen(str);
        if(str[len - 1] == 10)
                str[len - 1] = '\0';
        return str;
}

/*
 * Return: empty 0, end -1, other length
 */
int str_getline(char *str, char res[], int first)
{
        static int pos, len;
        int tpos;
        char line[MAX_LINE_LEN];

        if (first) {
                pos = 0;
                len = strlen(str);
        }

        if (len == 0)
                return 0;

        sscanf(str + pos, "%[^\n]", line);
        tpos = strlen(line);
        //printf("pos: %d, str: %s, linke : %s\n", pos, str, line);

        if (len == (pos - 1))
                return -1;

        if (tpos == 0)
                return 0;

        //printf("RES:%s\n", res);
        strncpy(res, str + pos, tpos);
        pos += tpos + 1;

        return tpos;
}

int read_file(const char *file, char **fbuf)
{
        int size;
        int fd;
        struct stat sb;

        if ((fd = open(file, O_RDONLY)) == -1) {
                fprintf(stderr, "Unable to open file: %s\n", file);
                return -1;
        }

        if (fstat(fd, &sb) == -1) {
                fprintf(stderr, "Unable to stat file: %s\n", file);
                return -1;
        }

        size = MIN(sb.st_size,MAX_READ_FILE_LEN);

        if (((*fbuf) = (char*)malloc(size + 1)) == NULL) {
                fprintf(stderr ,"Unable alloc memory for read_file\n");
                return -1;
        }

        return read(fd, (*fbuf), size);
}
