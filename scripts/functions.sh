#!/bin/sh

error_exit() {
  echo $*
  exit 1
}

#
# Proc info 
#
get_proc_pid() {
  if test -z "$1"; then
    error_exit "Argument proc name is missing!"
  fi

  ps aux |grep "$1" |awk '{print $2}' |tr -t '\n' ' '
}

proc_exists() {
  PROCS=`get_proc_pid $1`

  if test -z "$PROCS"; then
    echo -1
  else
    echo $PROCS | wc -w
  fi
}

get_proc_status() {
  user=""
  if test -z "$1"; then
    error_exit "Argument proc name is missing!"
  fi

  if test ! -z "$2"; then
    user="$2"
  fi

  if test ! -z "$user"; then
    for proc in `ps aux |grep $1 |grep ^$user | awk '{print $2}'`; do
      cat /proc/$proc/status |tr -t ' ' '\n'
      cat /proc/$proc/io
    done
  else
    for dir in `ls /proc |grep '^[0-9]'`; do
      match=`grep -i "$1*" /proc/$dir/comm`
      if test ! -z "$match"; then
        cat /proc/$dir/status
        cat /proc/$dir/io
      fi
    done
  fi
}

get_file_size_byreg() {
  if test -z "$1"; then
    error_exit "Argument regex is missing!"
  fi

  ls -l $1 |awk '{sum += $5} END {print sum}'
}

list_funcs() {
  grep '^[a-z_]*() {$' $1 |tr -d '(){' |tr -t ' ' '\n'
}

main() {
  if test -z "$1"; then
    error_exit "Argument func is missing!"
  elif test -z "$2"; then
    error_exit "Argument arg is missing!"
  fi

  $*
}

case "$1" in
  list_funcs)
    RET=`$1 $0`
    echo $RET
    ;;
  *)
    main $*
    ;;
esac
# vim: set sw=2 :
