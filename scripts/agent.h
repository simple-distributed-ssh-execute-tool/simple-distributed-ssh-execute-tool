#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAX_LINE_LEN    4096
#define MAX_FILE_LEN    409600

#define PROC_MEMINFO    "/proc/meminfo"

typedef unsigned long   ulong;
typedef unsigned int    uint;

typedef struct _file {
        FILE *fp;
        char name[128];
        int fd;
        struct stat sb;
        char *content;
        int (*readall) (struct _file *);
        int (*readline) (struct _file *, char**buf);
        int (*init) (const char *file, struct _file *);
        int (*close) (struct _file *);
} file;

/* kernel 3.2.0 */
typedef struct _meminfo {
        uint memtotal;
        uint memfree;
        uint buffers;
        uint cached;
        uint swapcached;
        uint active;
        uint inactive;
        uint activeanon;
        uint inactiveanon;
        uint activefile;
        uint inactivefile;
	uint unevictable;
	uint mlocked;
	uint swaptotal;
	uint swapfree;
	uint dirty;
	uint writeback;
	uint anonpages;
	uint mapped;
	uint shmem;
	uint slab;
	uint sreclaimable;
	uint sunreclaim;
	uint kernelstack;
	uint pagetables;
	uint nfs_unstable;
	uint bounce;
	uint writebacktmp;
	uint commitlimit;
	uint committed_as;
	uint vmalloctotal;
	uint vmallocused;
	uint vmallocchunk;
	uint hardwarecorrupted;
	uint anonhugepages;
	uint hugepages_total;
	uint hugepages_free;
	uint hugepages_rsvd;
	uint hugepages_surp;
	uint hugepagesize;
	uint directmap4k;
	uint directmap2m;
	uint directmap1g;
} meminfo;
