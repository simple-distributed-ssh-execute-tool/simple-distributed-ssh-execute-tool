#!/usr/bin/env perl

use strict;
use warnings;
use IO::Dir;



sub get_proc_status {
    my $name = shift;
    $name or die("Missing argument: proc name\n");

    my $d = IO::Dir->new('/proc/');
    if (defined $d) {
        while (defined(($_ = $d->read()))) {
            if (/^[0-9]+$/) {
                open CMFILE, "<", "/proc/". $_. "/comm" or die("Failed to open proc comm file\n");
                my $line = <CMFILE>;
                if (!defined($line) || $line !~ /$name/) {
                    close CMFILE;
                } else {
                    open STATFILE, "<", "/proc/". $_. "/status" or die("Failed to open proc status file\n");
                    while(<STATFILE>) {
                        if /^Pid/ {
                        } elsif /^Uid/ {
                        } elsif /^Gid/ {
                        } elsif /^VmPeak/ {
                        } elsif /^VmSize/ {
                        } elsif /^VmHWM/ {
                        } elsif /^VmRSS/ {
                        } elsif /^VmDATA/ {
                        } elsif /^VmStk/ {
                        } elsif /^Threads/ {
                        }
                    print @lines;
                    close STATFILE;
                    close CMFILE;
                }
            }
        }
        undef $d;
    }
}

get_proc_status("enlightenment");
