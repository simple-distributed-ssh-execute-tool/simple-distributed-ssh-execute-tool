#!/usr/bin/perl


$str = "key: 1\nkey2: 2\n";
@ary = split(/\n/, $str);

sub get_key {
    @ary = split (/:/);
    return @ary[0];
}
%hash = map { get_key($_) => $_ } @ary;
foreach $key (keys %hash) {
    print $key, '=>', $hash{$key};
}
