#include "agent.h"

static int _file_readline(file *file_obj, char **buf)
{
        if (file_obj->fp == NULL)
                return 1;
        if (!(*buf = (char*)malloc(sizeof(char)*MAX_LINE_LEN+1))) {
                fprintf(stderr, "Unable to alloc memory for fgets file: %s\n", file_obj->name);
                return -1;
        }

        if (fgets(*buf, MAX_LINE_LEN, file_obj->fp))
                return 0;

        return -1;
}

static int _file_readall(file *file_obj)
{
        if (file_obj->fp == NULL)
                return 1;

        size_t file_size;

        file_size = (file_obj->sb.st_size == 0 ||
                     file_obj->sb.st_size > MAX_FILE_LEN)?
                MAX_FILE_LEN : file_obj->sb.st_size;

        printf("%d\n", file_size);

        if (!(file_obj->content = (char *)malloc(sizeof(char)*file_size+1))) {
                fprintf(stderr, "Alloc memory for file read buf\n");
                return 2;
        }

        if (!fread(file_obj->content, sizeof(char), file_size, file_obj->fp)) {
                fprintf(stderr, "Fread error\n");
                return 3;
        }

        return 0;
}

static int _file_close(file *file_obj)
{
        if (file_obj->fp) {
                fclose(file_obj->fp);
        }

        if (file_obj->content != NULL)
                free(file_obj->content);

        return 0;
}


static int _file_init(const char *fn, file *file_obj)
{
        file_obj->fp = NULL;
        file_obj->content = NULL;
        strncpy(file_obj->name, fn, sizeof(file_obj->name)-1);

        if (!(file_obj->fp = fopen(fn, "r"))) {
                fprintf(stderr, "Unable open file: %s\n", fn);
                exit(-1);
        }

        if (stat(fn, &(file_obj->sb)) != 0) {
                fprintf(stderr, "Unable stat file: %s\n", fn);
                exit(-2);
        }

        file_obj->readall = &_file_readall;
        file_obj->readline = &_file_readline;
        file_obj->close = &_file_close;
        return 0;
}


static int get_meminfo(meminfo *minfo)
{
        char *buf;
        file file_obj;
        file_obj.init = &_file_init;
        file_obj.init("/proc/meminfo", &file_obj);
        char *tbuf;

        minfo->memtotal = 0;
        minfo->memfree = 0;
        while(file_obj.readline(&file_obj, &buf) == 0) {
                sscanf(buf, "%*s%s", tbuf);
                switch (buf[0]) {
                        case 'A':
                                // Active
                                if (buf[1] == 'n')
                                        minfo
                                if (buf[6] == ':')
                                        minfo->active = atol(tbuf);
                                else if (buf[6] == '(' && buf[7] == 'a')
                                        minfo->activeanon = atol(tbuf);
                                else if (buf[7] == 'f')
                                        minfo->activefile = atol(tbuf);
                                break;
                        case 'B':
                                // Bounce and Buffers
                                if (buf[1] == 'o')
                                        minfo->bounce = atol(tbuf);
                                else if (buf[1] == 'u')
                                        minfo->buffers = aotl(tbuf);
                                break;
                        case 'C':
                                // Cached, CommitLimit, Committed_AS
                                if (buf[1] == 'a')
                                        minfo->cached = atol(tbuf);
                                else if (buf[6] == 'L')
                                        minfo->commitlimit =  atol(tbuf);
                                else if (buf[6] == 't')
                                        minfo->committed_as = atol(tbuf);
                                break;
                        case 'D':
                                // DirectMap1G(2M,4k), Dirty
                                if (buf[5] == 't') {
                                        if (buf[10] == 'G')
                                                minfo->directmap1g = atol(tbuf);
                                        else if (buf[10] == 'M')
                                                minfo->directmap2m = atol(tbuf);
                                        else if (buf[10] == 'k')
                                                minfo->directmap4k = atol(tbuf);
                                } else if (buf[4] == 'y')
                                        minfo->dirty = atol(tbuf);
                                break;
                        case 'H':


        }

        file_obj.close(&file_obj);
        return 0;
}

int main(int argc, char *argv[])
{
        file file_obj;
        file_obj.init = &_file_init;
        char *buf;
        meminfo minfo;

        file_obj.init("/proc/cpuinfo", &file_obj);

        while (file_obj.readline(&file_obj, &buf) == 0) {
                printf("%s", buf);
        }

        file_obj.close(&file_obj);

        get_meminfo(&minfo);
        /*
        file_obj.readall(&file_obj);

        if (file_obj.content) {
                printf("file : /proc/cpuinfo, content: %s\n", file_obj.content);
        }
        */

        return 0;
}
