#!/usr/bin/env python

from __future__ import with_statement
import os
import sys
import fnmatch
import re
import subprocess
import getopt
import tarfile
import pwd
import grp
import spwd

THREAD_WAR_VALUE = 30
MEM_WAR_VALUE = 2.0
SWAP_WAR_VALUE = 800.0

def user_exists(username=None):
    if username is None:
        return None

    try:
        entry = pwd.getpwnam(username)
    except (RuntimeError, TypeError, NameError, KeyError):
        return None

    if len(entry) > 0:
        return entry
    return None

def lock_user(username=None):
    if username is None:
        return 1

    if user_exists

def console_color(color=None):
    if color is None:
        print "Missing argument: color\n"
        return ''

    if color == 'white':
        return '\033[22;37m'
    elif color == "black":
        return "\033[22;30m"
    elif color == "green":
        return "\033[01;32m"
    elif color == "red":
        return "\033[01;31m"
    elif color == "cyan":
        return "\033[22;36m"

def get_proc_status(name=None,doprint=1):

    data = []

    for f in os.listdir('/proc/'):
        _data = {}
        if fnmatch.fnmatch(f, '[0-9]*'):
            path_comm = os.path.join('/proc/', f, 'comm')
            if not os.access(path_comm, os.R_OK):
                continue

            if name is not None:
                with open(path_comm) as fp:
                    if cmp(fp.readline(),name + "\n") != 0:
                        continue

            path_status = os.path.join('/proc/', f, 'status')
            if not os.access(path_status, os.R_OK):
                    continue

            with open(path_status) as fp:
                for line in fp:
                    fields = line.split(':')
                    _data.update({fields[0].strip() : fields[1].strip().replace('kB','')})
            data.append(_data)

    if doprint:
        print_proc_data(data)

    return data

def get_meminfo():
    data = {}
    with open('/proc/meminfo') as fp:
        for line in fp:
            fields = line.split(':')
            data.update({fields[0].strip(): fields[1].strip().replace('kB','')})

    return data

def print_proc_data(data=None):
    mem_data = get_meminfo()
    if 'MemTotal' not in mem_data:
        print 'Unable get meminfo\n'
        return

    for _data in data:
        if 'Name' in _data:
            print console_color('green')+_data['Name'],
        if 'State' in _data:
            print console_color('red')+_data['State']+' ',
        if 'Threads' in _data:
            if int(_data['Threads']) > THREAD_WAR_VALUE:
                print console_color('red'),
            else:
                print console_color('white'),
            print 'threads:'+_data['Threads']+' ',
        if 'voluntary_ctxt_switches' in _data:
            print console_color('white')+'vcs'+':'+ _data['voluntary_ctxt_switches']+' ',
        if 'nonvoluntary_ctxt_switches' in _data:
            print console_color('white')+'nvcs'+':'+ _data['nonvoluntary_ctxt_switches']+' ',
        if 'VmRSS' in _data:
            if int(mem_data['MemTotal']) / float(_data['VmRSS']) < MEM_WAR_VALUE:
                print console_color('red'),
            print "%s:%s" %('vmrss', _data['VmRSS']),
        if 'VmHWM' in _data:
            if int(mem_data['MemTotal']) / float(_data['VmHWM']) < MEM_WAR_VALUE:
                print console_color('red'),
            print "%s:%s\n" %('vmhwm', _data['VmHWM']),
        if 'VmPeak' in _data:
            if int(mem_data['MemTotal']) / float(_data['VmPeak']) < MEM_WAR_VALUE:
                print console_color('red'),
            print "%s:%s" %('vmpeak', _data['VmPeak']),
        if 'VmSize' in _data:
            if int(mem_data['MemTotal']) / float(_data['VmSize']) < MEM_WAR_VALUE:
                print console_color('red'),
            print "%s:%s" %('vmsize', _data['VmSize']),
        if 'VmSwap' in _data:
            if int(mem_data['SwapTotal']) / max(1,float(_data['VmSwap'])) < SWAP_WAR_VALUE:
                print console_color('red'),
            print "%s:%s" %('vmswap', _data['VmSwap']),
        print ''

def check_raid_status():
    raid_tools = ['/usr/sbin/hpacucli', '/sbin/hpacucli', '/opt/MegaRAID/MegaCli/MegaCli', '/usr/sbin/megaraidsas-status', '/usr/sbin/megaraid-status', '/bin/dmesg']

    found = 0
    for cmd in raid_tools:
        if not os.access(cmd, os.X_OK):
            continue
        found = 1
        break

    if not found:
        print "Unable found raid tool!"
        return 1

    if cmd[-4:] == 'ucli':
        cmd += " ctrl all show config"
    elif cmd[-4:] == 'mesg':
        cmd += " | tail -n20"

    ret = {}
    ret = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    stdo = ret.stdout.readlines()
    erro = ret.stderr.readlines()
    if len(erro) > 0:
        print erro
        return 2

    print ''.join(['%s' % (k) for k in stdo]),
    return 0

def add_ip_route(ip=None,mask=None,dev=None,label=None,route=None):
    if not ip:
        print 'Argument missing: ip'
        sys.exit(2)

    if not re.match(r"^(\d+)\.(\d+)\.(\d+)\.(\d+)$", ip) or not re.match(r"^[a-z0-9]+$", dev):
        print 'Invalid ip format!'
        sys.exit(2)

    if not mask:
        mask = 24
    if not dev:
        dev = "eth0"
    if not label:
        label = "eth0:1"

    bin_ip = '/sbin/ip addr add %s/%d dev %s label %s' %(ip,mask,dev,label)

def get_opt():
    try:  
        opts, args = getopt.getopt(sys.argv[1:], "ha:c:", ["help", "args=",'command='])  
    except getopt.GetoptError as err:  
        print str(err)
        usage()
        sys.exit(2)

    func = None
    args = ''
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif o in ("-c", "--command"):
            func = a
        elif o in ("-a", "--args"):
            args = str(a)
        else:
            assert False, "unhandled option"

    if not func:
        usage()
        sys.exit(2)
        
    func = func.replace('.', '_')

    old_args = args
    len_args = len(args)

    if len_args > 0:
        if not re.match(r'^["a-zA-Z0-9,:-]+$', old_args):
            print "Argument %s format is error!" %(old_args)
            sys.exit(3)

        args = args.replace(':', '=')
#        re.sub('=([a-zA-Z]+[0-9]*)(\,?)', '="\1"\2', args)

    if len_args == 0:
        callstr = func
    else:
        callstr = func + "(" + args + ")"

    print callstr
    call = eval(callstr)
    if len_args == 0:
        call()
    '''
    arglist = args.split(",")
    arglist = map(lambda x: x.replace(':', '='), arglist)
    argstr = ''.join(',%s' %(k) for k in arglist)
    print argstr
    '''

def usage():
    print 'Usage ./base.py -c "command" -a "args"\n'\
        ' -h\t\thelp\n'\
        ' -c|--command\tcommand\n'\
        ' -a|--args\t"arg1:val,arg2:val"'

def exec_cmd(cmd=None):
    if not cmd:
        return None 

    ret = {}
    ret = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    stdo = ret.stdout.readlines()
    erro = ret.stderr.readlines()
    if len(erro) > 0:
        print erro
        return None 

    print ''.join(['%s' % (k) for k in stdo]),
    return ret

def tar_create():
    return

if __name__ == '__main__':
    get_opt()
    '''
    print_data(get_proc_status(None))
    print_data(get_proc_status("enlightenment"))
    check_raid_status()
    '''
