#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>

#include "error.h"


void err_exit(const char* fmt, ...)
{
        int n;
        va_list ap;

        char msg[MAX_MSG_LEN + 1];

        va_start(ap, fmt);
        n = vsnprintf(msg, MAX_MSG_LEN, fmt, ap);
        va_end(ap);
        if (n <= -1)
                exit(4);

        fprintf(stderr, "%s", msg);
        exit(4);
}

