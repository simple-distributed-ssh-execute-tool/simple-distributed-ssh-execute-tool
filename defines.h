#ifndef DEFINES_INCLUDED
#define DEFINES_INCLUDED

#include <libssh/libssh.h>

#define MAX_READ_FILE_LEN       102400
#define MAX_LOG_BUF             4096
#define MAX_PATH_LEN            255
#define MAX_UNAME_LEN           30
#define MAX_FILE_LEN            1024
#define MAX_LINE_LEN            4096

#define MAX_RECEIVE_LEN         20480

#define FILE_POOL_LOG           0
#define MAX_CONCURRENT          100

#define LOG_FILE_PREFIX         "log_mcr_"
#define CMD_FILE_INFO           "./cmd.info"

#define ENCRYPT_METHOD          "blowfish"
#define ENCRYPT_MODE            "cfb"
#define VIM_ENCRYPT_PRE         "VimCrypt~02!"

#define MIN_PASS_LEN            8
#define MAX_PASS_LEN            64

#define MCR_MSG_ERR_FATAL       0
#define MCR_MSG_WARING          1
#define MCR_MSG_NORMAL          2
#define MCR_MSG_RESULT          3

#define MCR_DEBUG_L0            0
#define MCR_DEBUG_L1            1
#define MCR_DEBUG_L2            2
#define MCR_DEBUG_L3            3

#define MAX_MSG_LEN             255

#define MIN(a,b)               ((a)<=(b)?(a):(b))
#define MAX(a,b)               ((a)>=(b)?(a):(b))

typedef struct _options {
        char *crypt_str;
        char *arg;
        char *internal_cmd;
        char *targ_path;
        char *arg_cmd;
        char *src_file;
        char *targ_owner;
        char *targ_mode;
        char *group;
        char *exec_type;
        char *keys_dir;
        char *decrypt;
        char *dir_nomatch;
        int debug;
        int vim_crypt;
        int hosts_encrypted;
        int allmsg;
        int exec_script;
} options;

struct ssh_sess {
        ssh_session sess;
        char *host;
};

struct ssh_scp {
        ssh_scp scp;
        char *host;
};

struct sarg {
        char *user;
        char *passwd;
        char *command;
        char *host;
        char *private_key;
        char *private_pass;
        char *arg;
        char group[64];
        int port;
        char *script_file;
        struct sarg* next;
};

typedef struct sarg real_ssh_arg;


#endif
