#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netdb.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdarg.h>
#include <time.h>
#include <signal.h>
#include <dirent.h>
#include <libgen.h>

#include "defines.h"
#include "buf.h"
#include "crypto.h"
#include "error.h"

#include <libssh/libssh.h>

const char *g_tpl_expect_ssh_sudo = "#!/usr/bin/expect -f\n"\
                                     "set pass [lindex $argv 0]\n"\
                                     "spawn %s \nexpect \": \"\n"\
                                     "send -- \"$pass\\r\"\nexpect eof";

static options opt;
struct ssh_sess ssh_sess_pool[MAX_CONCURRENT];
static int ssh_sess_num = 0;
struct ssh_scp ssh_scp_pool[MAX_CONCURRENT];
static int ssh_scp_num = 0;
static int __err_num;
static char*  __err[100];
static int __sig_num;

static void console_color(char *color);
static void msg(int msg_type, const char *fmt, ...);
static char *oitoa(int src, int max);
static char *decrypt_file(const char *file, int print);
static char * encrypt_file(char *file);
static void ask_exit();
static char* now(char * now_tm);
static char* parse_cmd(const char *cmd, int field);
static void add_ssh_err(char *host, char *msg);
static void mcr_log(const char* log_host, size_t size, const char *fmt, ...);
static int verify_knownhost(ssh_session session);
static void thelp();
static void close_ssh_scp(ssh_scp scp);
static int create_ssh_scp(ssh_session sess, char * host, int mode, const char* dist_path);
static int ssh_do_scp(char * host, ssh_session sess, const char *src, const char *dst_path);
static int ssh_remote_exec(ssh_session session, char *command, real_ssh_arg *arg, int log_cmd);
static int find_ssh_conn(char *host);
static void close_ssh_sess(id);
static int create_ssh_conn(void *ptr);
static int exec_ssh_cmd(void *ptr, int log_cmd);
static int exec_expect(void *ptr);
static int create_pool(int pool_size, char *command, char *type, int port, real_ssh_arg *rsarg);
static int mcr_parse_hosts_fcmd(char * hosts, int *hosts_num, real_ssh_arg *rsarg);
static real_ssh_arg * mcr_parse_hosts_ffile(char *file, int *hosts_num);
static int process_cmd(char *command,char *type, char *cfg_file, char *hosts, int port, int concurrent); 
static void _signal_proc(int signo);
static void signal_proc();
static void add_ssh_keys();
static void handle_crypt();
static int validate_decrypted(const char *decrypted);
static int validate_char(const char *str, int len);
static void init_options();

int main(int argc, char *argv[])
{
        char *opt_command, *opt_cfg_file, *opt_hosts, *opt_type, 
             *opt_hosts_file;
        char c;
        int opt_debug, opt_concurrent, opt_port;

        init_options();

        opt_command = opt_cfg_file = "";
        opt_type = "real";
        opt_debug = 0;
        opt_port = 22;
        opt_hosts = "";
        opt_hosts_file = "";
        opt.keys_dir = "";
        opt.arg = "";
        opt.group = "";

        if (argc <= 1)
                thelp();

        static struct option long_options[] = {
                {"cfg-file", 1, 0, 0},
                {"hosts", 1, 0, 0},
                {"port", 1, 0, 0},
                {"concurrent", 1, 0, 0},
                {"type", 1, 0, 0},
                {"src-file", 1, 0, 0},
                {"targ-path", 1, 0, 0},
                {"targ-mode", 1, 0, 0},
                {"targ-owner", 1, 0, 0},
                {"keys-dir", 1, 0, 0},
                {"vim-crypt", 1, 0, 0},
                {"all-msg", no_argument, 0, 0},
                {0, 0, 0, 0}
        };

        for (;;) {
                int option_index = 0;

                c = getopt_long(argc, argv, "f:c:d:ht:n:i:a:r:u:eg:s",
                                long_options, &option_index);
                if (c == -1)
                        break;

                char *t;
                switch (c) {
                        case 0:
                                t = (char *) long_options[option_index].name;
                                if (strcmp(t, "cfg-file") == 0) {
                                        opt_cfg_file = optarg;
                                        /* file exists? */
                                        if (access(opt_cfg_file, 0) != 0) 
                                                err_exit("Could not found configure file!\n");
                                } else if (strcmp(t, "hosts") == 0)
                                        opt_hosts = optarg;
                                else if (strcmp(t, "port") == 0)
                                        opt_port = atoi(optarg);
                                else if (strcmp(t, "type") == 0)
                                        opt_type = optarg;
                                else if (strcmp(t, "src-file") == 0)
                                        opt.src_file = mk_string(opt.src_file,
                                                                   optarg,
                                                                   strlen(optarg));
                                else if (strcmp(t, "targ-path") == 0)
                                        opt.targ_path = mk_string(opt.targ_path,
                                                                    optarg,
                                                                    strlen(optarg));
                                else if (strcmp(t, "targ-owner") == 0)
                                        opt.targ_owner = mk_string(opt.targ_owner,
                                                                     optarg,
                                                                     strlen(optarg));
                                else if (strcmp(t, "targ-mode") == 0)
                                        opt.targ_mode = mk_string(opt.targ_mode,
                                                                    optarg,
                                                                    strlen(optarg));
                                else if (strcmp(t, "keys-dir") == 0)
                                        opt.keys_dir = mk_string(opt.keys_dir,
                                                                   optarg,
                                                                   strlen(optarg));
                                else if (strcmp(t, "vim-crypt") == 0)
                                        opt.vim_crypt = atoi(optarg);
                                else if (strcmp(t, "all-msg") == 0)
                                        opt.allmsg = 1;

                                break;
                        case 'h':
                                thelp();
                                break;
                        case 's':
                                opt.exec_script = 1;break;
                        case 'd':
                                opt_debug = atoi(optarg);
                                opt.debug = atoi(optarg);
                                break;

                        case 't':
                                opt.exec_type = optarg;
                                break;

                        case 'f':
                                opt_hosts_file = optarg;
                                break;

                        case 'c':
                                opt_command = optarg;
                                opt.arg_cmd = mk_string(opt.arg_cmd,
                                                          optarg,
                                                          strlen(optarg));
                                break;

                        case 'n':
                                opt.dir_nomatch = mk_string(opt.dir_nomatch,
                                                              optarg,
                                                              strlen(optarg));
                        case 'i':
                                opt.internal_cmd = mk_string(opt.internal_cmd,
                                                               optarg,
                                                               strlen(optarg));
                                break;

                        case 'a':
                                opt.arg = mk_string(opt.arg,
                                                    optarg,
                                                    strlen(optarg));
                                break;

                        case 'r':
                                opt.crypt_str = mk_string(opt.crypt_str,
                                                            optarg,
                                                            strlen(optarg));
                                break;

                        case 'u':
                                opt.decrypt = mk_string(opt.decrypt,
                                                          optarg,
                                                          strlen(optarg));
                                break;

                        case 'e':
                                opt.hosts_encrypted = 1;
                                break;

                        case 'g':
                                opt.group = mk_string(opt.group,
                                                        optarg,
                                                        strlen(optarg));
                                break;

                        default:
                                thelp();
                                break;
                }
        }

        __err_num = 0;
        __sig_num = 0;
        signal_proc();

        if ((opt.crypt_str != NULL && (strcmp(opt.crypt_str, "") != 0)) ||
            (opt.decrypt != NULL && strcmp(opt.decrypt, "") != 0)) {
                handle_crypt();
                return 0;
        }

        if (strcmp(opt.keys_dir, "") > 0) {
                add_ssh_keys();
                while (wait(NULL) > 0);
                msg(MCR_MSG_NORMAL, "All Done.\n");
                return 0;
        }

        opt_concurrent = 100;
        process_cmd(opt.internal_cmd ? parse_cmd(opt.internal_cmd, 1) :
                    opt_command,
                    opt_type, opt_cfg_file,
                    opt_hosts_file != NULL ? opt_hosts_file : opt_hosts,
                    opt_port, opt_concurrent);

        while (wait(NULL) > 0);
        msg(MCR_MSG_NORMAL, "All Done!\n");
        return 0;
}

static void console_color(char *color)
{
        if (strcmp(color, "white") == 0)
                printf("\033[22;37m\n");
        else if (strcmp(color, "black") == 0)
                printf("\033[22;30m");
        else if (strcmp(color, "green") == 0)
                printf("\033[01;32m");
        else if (strcmp(color, "red") == 0)
                printf("\033[01;31m");
        else if (strcmp(color, "cyan") == 0)
                printf("\033[22;36m");
}

static void msg(int msg_type, const char *fmt, ...)
{
        int n;
        va_list ap;

        if (opt.debug == 0 || 
	    (opt.debug == 1 && msg_type == MCR_MSG_NORMAL))
                return;

        char msg[MAX_MSG_LEN];

        va_start(ap, fmt);
        n = vsnprintf(msg, MAX_MSG_LEN, fmt, ap);
        va_end(ap);

        if (n <= -1)
                return;

        char *info[] = {
                "[ FATAL ERROR ]",
                "[ WARNING ]",
                "[ INFO ] ",
                "[ RESULT ] ",
        };

        if (msg_type > MCR_MSG_RESULT || msg_type < MCR_MSG_ERR_FATAL)
                msg_type = MCR_MSG_NORMAL;

        switch (msg_type) {
                case MCR_MSG_ERR_FATAL:
                case MCR_MSG_WARING:
                        console_color("red");
                        break;
                default:
                        console_color("green");
        }
        printf("%s ", info[msg_type]);
        console_color("white");
        if (msg_type == MCR_MSG_ERR_FATAL)
                printf("! ");
        else
                printf("* ");
        printf("%s", msg);
}

static char *oitoa(int src, int max)
{
        char *str;

        str = (char *)malloc(max);
        snprintf(str, max, "%d", src);
        return str;
}



static char* now(char * now_tm)
{
        char *wday[]={"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
        time_t timep;
        struct tm *p;
        if ((now_tm = malloc(sizeof(now_tm) * 100)) == NULL)
                err_exit("Could not alloc memory: now()!\n");
        time(&timep);
        p = localtime(&timep);
        snprintf(now_tm, 100, "%d/%d/%d %s %d:%d:%d", (1900 + p->tm_year),
                 p->tm_mon, p->tm_mday, wday[p->tm_wday],
                 p->tm_hour, p->tm_min, p->tm_sec);
        return now_tm;
}


static void add_ssh_err(char *host, char *msg)
{
        char buf[255];
        snprintf(buf, 255, "%s: %s", host, msg);
        __err[__err_num] = mk_string(__err[__err_num],
                                     buf,
                                     strlen(buf));
        __err_num += 1;
}

static void mcr_log(const char* log_host, size_t size, const char *fmt, ...)
{
        FILE *log_fp;
        int n;
        char log_file[255] = LOG_FILE_PREFIX;
        int len;
        va_list ap;
        char *msg;

        if (size == 0)
                size = MAX_LOG_BUF;


        strncat(log_file, log_host, 200);

        if ((log_fp = fopen(log_file, "a+")) == NULL) {
                err_exit("Could not write log file, create");
        }

        if ((msg = (char *)malloc(size)) == NULL) {
                fprintf(stderr, "Can not alloc memory: mcr_log!\n");
                return;
        }

        va_start(ap, fmt);
        n = vsnprintf(msg, size, fmt, ap);
        va_end(ap);

        if (n <= -1) {
                free(msg);
                err_exit("Could not create msg!");
        }

        if (opt.debug > 2)
                fprintf(stdout, "**DEBUG %s", msg);

        len = strlen(msg);
        if (fwrite(msg, sizeof(char), len, log_fp) != len) {
                free(msg);
                err_exit("Could not write to log file");
        }

        free(msg);
        fclose(log_fp);
}

static int verify_knownhost(ssh_session session)
{
        int state, hlen;
        unsigned char *hash = NULL;
        char *hexa;
        char buf[10];

        state = ssh_is_server_known(session);

        hlen = ssh_get_pubkey_hash(session, &hash);
        if (hlen < 0)
                return -1;

        switch (state) {
                case SSH_SERVER_KNOWN_OK:
                        break; /* ok */

                case SSH_SERVER_KNOWN_CHANGED:
                        fprintf(stderr, "Host key for server changed: it is now:\n");
                        ssh_print_hexa("Public key hash", hash, hlen);
                        fprintf(stderr, "For security reasons, connection will be stopped\n");
                        free(hash);
                        return -1;

                case SSH_SERVER_FOUND_OTHER:
                        fprintf(stderr, "The host key for this server was not found but an other"
                                "type of key exists\n");
                        fprintf(stderr, "An attacker might change the default server key to"
                                "confuse your client into thinking the key does not exist\n");
                        free(hash);
                        return -1;

                case SSH_SERVER_FILE_NOT_FOUND:
                        fprintf(stderr, "Could not find known host file.n");
                        fprintf(stderr, "If you accept the host key here, the file will be"
                                "automatically created.\n");
                        /* fallback to SSH_SERVER_NOT_KNOWN behavior */

                case SSH_SERVER_NOT_KNOWN:
                        hexa = ssh_get_hexa(hash, hlen);
                        fprintf(stderr,"The server is unknown. Do you trust the host key?\n");
                        fprintf(stderr, "Public key hash: %s\n", hexa);
                        free(hexa);
                        if (fgets(buf, sizeof(buf), stdin) == NULL) {
                                free(hash);
                                return -1;
                        }
                        if (strncasecmp(buf, "yes", 3) != 0) {
                                free(hash);
                                return -1;
                        }
                        if (ssh_write_knownhost(session) < 0) {
                                fprintf(stderr, "Error %sn", strerror(errno));
                                free(hash);
                                return -1;
                        }
                        break;

                case SSH_SERVER_ERROR:
                        fprintf(stderr, "Error %s", ssh_get_error(session));
                        free(hash);
                        return -1;
        }

        free(hash);
        return 0;
}


static void close_ssh_scp(ssh_scp scp)
{
        ssh_scp_close(scp);
        ssh_scp_free(scp);
}

static int create_ssh_scp(ssh_session sess, char * host,
                          int mode, const char* dist_path)
{
        int id;
        
        /*
        if ((id = find_ssh_scp(host)) != -1)
                return id;

                */
        id = ssh_scp_num++;

        if (id >= MAX_CONCURRENT) {
                printf("Could not create scp session\n");
                return -1;
        }

        if ((ssh_scp_pool[id].scp = ssh_scp_new(sess, mode, dist_path)) == NULL) {
                printf("Could not create new scp session\n");
                return -1;
        }

        if (ssh_scp_init(ssh_scp_pool[id].scp) != SSH_OK) {
                fprintf(stderr, "Error initializing scp session: %s\n",
                        ssh_get_error(sess));
                ssh_scp_free(ssh_scp_pool[id].scp);
                return -1;
        }
        ssh_scp_pool[id].host = host;

        return id;
}

static int ssh_do_scp(char * host, ssh_session sess, const char *src,
                      const char *dst_path)
{
        char *fbuf;
        int id;
        int size;

        if ((id = create_ssh_scp(sess, host, SSH_SCP_WRITE, dst_path)) == -1) {
                fprintf(stderr, "Failed create ssh scp\n");
                return -1;
        }

        /*
        if (ssh_scp_push_directory(scp, "helloworld", S_IRWXU) != SSH_OK) {
                fprintf(stderr, "Can't create remote directory: %s\n",
                        ssh_get_error(session));
                ssh_scp_free(scp);
                return -1;
        }
        */


        if ((size = read_file(src, &fbuf)) == - 1 || size == 0)
                return -1;

        if (ssh_scp_push_file(ssh_scp_pool[id].scp,
                              ssh_basename(src),
                              size,
                              S_IRWXU) != SSH_OK) {
                free(fbuf);
                fprintf(stderr, "Can't open remote file: %s\n",
                        ssh_get_error(sess));
                return -1;
        }

        if (ssh_scp_write(ssh_scp_pool[id].scp, fbuf, size) != SSH_OK) {
                fprintf(stderr,
                        "Can't write to remote file: %s\n",
                        ssh_get_error(sess));
                free(fbuf);
                return -1;
        }

        free(fbuf);
        close_ssh_scp(ssh_scp_pool[id].scp);
        return 0;
}

static int ssh_remote_exec(ssh_session session, char *command,
                           real_ssh_arg *arg, int log_cmd)
{
        ssh_channel channel;
        int rc;

        if ((channel = channel_new(session)) == NULL) {
                printf("%s: Can not create ssh channel!\n", arg->host);
                return SSH_ERROR;
        }

        if ((rc = channel_open_session(channel) != SSH_OK)) {
                printf("%s: Can not open ssh session!\n", arg->host);
                channel_close(channel);
                channel_free(channel);
                return rc;
        }

        rc = channel_request_exec(channel, command);
        if (rc != SSH_OK) {
                printf("%s: Can not exec ssh cmd!\n", arg->host);
                channel_close(channel);
                channel_free(channel);
                return rc;
        }

        char buffer[8192];
        unsigned int nbytes;
        char *now_tm = NULL;
        int len, size;
        char *msg_buf;
        int cmd_err = 0;

        now_tm = now(now_tm);
        mcr_log(arg->host, 0, "** [ %s ]\n** Result:\n", now_tm);
        nbytes = channel_read(channel, buffer, sizeof(buffer), 0);
        if (nbytes == 0) {
                nbytes = channel_read(channel, buffer, sizeof(buffer), 1);
                cmd_err = 1;
        }

        len = strlen(buffer);

	msg(MCR_MSG_RESULT, "%s@ [ %s ] `%s`:\n",
	    arg->host, now_tm, log_cmd ? arg->command : "Hidden");

        size = strlen(arg->host);
        unsigned long total_size = 0;

        if (opt.allmsg) {
                msg_buf = (char *)malloc(MAX_RECEIVE_LEN);
                memset(msg_buf, 0, MAX_RECEIVE_LEN);
        }

        while (nbytes > 0) {
                if (!opt.allmsg) {
                        console_color("green");
                        printf("[ %s ]\n", arg->host);
                        console_color("white");

                        if (write(1, buffer, nbytes) != nbytes) {
                                channel_close(channel);
                                channel_free(channel);
                                return SSH_ERROR;
                        }
                } else {
                        total_size += nbytes;
                        if (total_size > MAX_RECEIVE_LEN - 2) {
                                msg(MCR_MSG_NORMAL, "Receive buffer is full!\n");
                                return SSH_OK;
                        }

                        msg_buf = strncat(msg_buf, buffer, nbytes);
                }
                mcr_log(arg->host, nbytes, "%s", buffer);
                nbytes = channel_read(channel, buffer, sizeof(buffer), 0);
        }

        if (nbytes < 0) {
                add_ssh_err(arg->host, "SSH channel read error!\n");
                channel_close(channel);
                channel_free(channel);
                return SSH_ERROR;
        }

        if (opt.allmsg) {
                if (opt.allmsg) {
                        if (cmd_err)
                                console_color("red");
                        else 
                                console_color("green");
                        printf("[ %s ]\n", arg->host);
                        console_color("white");
                }
                fprintf(stdout, "%s\n", msg_buf);
                if (msg_buf)
                        free(msg_buf);
        }

        channel_send_eof(channel);
        channel_close(channel);
        channel_free(channel);
        
        return SSH_OK;
}

static int find_ssh_conn(char *host)
{
        int i;
        for (i = 0; i < MAX_CONCURRENT; i++) {
                if (ssh_sess_pool[i].sess != NULL &&
                    strcmp(ssh_sess_pool[i].host, host) == 0)
                        return i;
        }
        return -1;
}

static void close_ssh_sess(id)
{
        if (ssh_sess_pool[id].sess == NULL)
                return;

        ssh_disconnect(ssh_sess_pool[id].sess);
        ssh_free(ssh_sess_pool[id].sess);
}

static int create_ssh_conn(void *ptr)
{
        int id;
        int verbosity = SSH_LOG_NOLOG; 

//        int verbosity = SSH_LOG_PROTOCOL;
//        int verbosity = SSH_LOG_FUNCTIONS;


        real_ssh_arg *arg;
        arg = (real_ssh_arg *)ptr;

        if ((id = find_ssh_conn(arg->host)) != -1)
                return id;

        id = ssh_sess_num++;

        if (id >= MAX_CONCURRENT) {
                fprintf(stderr, "SSH session exceed: %d\n", MAX_CONCURRENT);
                return -1;
        }

        ssh_sess_pool[id].sess = ssh_new();
        ssh_sess_pool[id].host = arg->host;

        if (ssh_sess_pool[id].sess == NULL)
                err_exit("SSH init error!\n");

        ssh_options_set(ssh_sess_pool[id].sess, SSH_OPTIONS_HOST, arg->host);
        ssh_options_set(ssh_sess_pool[id].sess, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
        ssh_options_set(ssh_sess_pool[id].sess, SSH_OPTIONS_USER, arg->user);
        ssh_options_set(ssh_sess_pool[id].sess, SSH_OPTIONS_PORT, &arg->port);

        if (ssh_connect(ssh_sess_pool[id].sess) != SSH_OK) {
                fprintf(stderr, "Error connecting to %s: %s\n",
                        arg->host,
                        ssh_get_error(ssh_sess_pool[id].sess));
                close_ssh_sess(id);
                return -1;
        }

        if (verify_knownhost(ssh_sess_pool[id].sess) < 0) {
                close_ssh_sess(id);
                return -1;
        }

        if (strlen(arg->passwd) > 1) {
                // Password auth
                if (ssh_userauth_password(ssh_sess_pool[id].sess, NULL, arg->passwd)) {
                        msg(MCR_MSG_ERR_FATAL, "Host: %s, auth failed: %s\n",
                            arg->host, ssh_get_error(ssh_sess_pool[id].sess));
                        close_ssh_sess(id);
                        return -1;
                }
        } else if (strlen(arg->private_key) > 1) {
                if (ssh_userauth_privatekey_file(ssh_sess_pool[id].sess,
                                                 NULL,
                                                 arg->private_key,
                                                 arg->private_pass)) {
                        /*
                                                 privatekey_from_file(
                                                                      ssh_sess_pool[id].sess,
                                                                      NULL,
                                                                      arg->private_key,
                                                                      arg->private_pass))) { */
                        msg(MCR_MSG_ERR_FATAL, "Host: %s, auth failed: %s\n",
                            arg->host, ssh_get_error(ssh_sess_pool[id].sess));
                        close_ssh_sess(id);
                        return -1;
                }
        } else {
                if (ssh_userauth_autopubkey(ssh_sess_pool[id].sess, NULL)) {
                        msg(MCR_MSG_ERR_FATAL, "Host: %s, auth failed: %s\n",
                            arg->host, ssh_get_error(ssh_sess_pool[id].sess));
                        close_ssh_sess(id);
                        return -1;
                }
        }

        return id;
}

static int exec_ssh_cmd(void *ptr, int log_cmd)
{
        real_ssh_arg *arg;
        char *now_tm;

        arg = (real_ssh_arg *)ptr;
        mcr_log(arg->host, 0, "Execute '%s' at host: %s, port: %d.\n",
                log_cmd ? arg->command : "Hidden", arg->host, arg->port);
                
        int id;
        char *exec_path = NULL;

        if ((id = create_ssh_conn(arg)) == -1) {
                msg(MCR_MSG_ERR_FATAL, "%s\n",
                    "Could not create ssh connection");
                return -1;
        }

        if (opt.exec_script ||
            (opt.targ_path != NULL && strcmp(opt.targ_path, "") != 0)) {
                if (strcmp(opt.src_file, "") == 0) {
                        printf("::Error: src file is empty!\n");
                        return -1;
                }
                now_tm = NULL;
                now_tm = now(now_tm);
                mcr_log(arg->host, 0,
                        "\n** [ %s ]\n** Result:\n", now_tm);
                int alen = 0;
                int llen = 0;
                char *t;
                if (opt.exec_script) {
                        t = basename(opt.src_file);
                        llen = strlen(t);
                        if (opt.arg)
                                alen = strlen(opt.arg);

                        llen += alen + 10;
                        exec_path = mk_string(exec_path, NULL, llen+1);
                        snprintf(exec_path, llen,
                                 "~/.dmcr_%s", t);
                        if (opt.targ_path)
                                free(opt.targ_path);
                        opt.targ_path = exec_path;
                }

                mcr_log(arg->host, 0,
                        "Copy file from '%s' to remote: %s:%s...\n",
                        opt.src_file, opt.targ_path, arg->host);
                if (ssh_do_scp(arg->host, ssh_sess_pool[id].sess, opt.src_file,
                               opt.targ_path) == 0) {
                        mcr_log(arg->host, 0, " Done.\n", "");
                        if (opt.exec_script) {
                                if (alen) {
                                        memset(exec_path, 0, llen+1);
                                        snprintf(exec_path, llen,
                                                 "~/.dmcr_%s %s", t, opt.arg);
                                }
                                /* execute script */
                                ssh_remote_exec(ssh_sess_pool[id].sess,
                                                exec_path,
                                                arg,
                                                log_cmd);
                        }
                        return 0;
                } else {
                        mcr_log(arg->host, 0, " Failed.\n", "");
                        return -2;
                }
        } else
                ssh_remote_exec(ssh_sess_pool[id].sess,
                                arg->command, arg, log_cmd);

/*        close_ssh_sess(id); */
        return 0;
}

static int exec_expect(void *ptr)
{
        real_ssh_arg * arg;
        arg = (real_ssh_arg *)ptr;
        char buf[255], buf1[255];
        char tbuf[64];
        FILE *fp;
        const char *tmp_file = "/tmp/.%s.expect.sh";
        char fn[64];
        int len;

        printf("Using exec_expect, %s, %s, %d\n", arg->host,
               arg->command, arg->port);

        if (strcmp(arg->script_file, "") != 0) {
                snprintf(tbuf, 64, "/tmp/%s %s",
                         basename(arg->script_file),
                         opt.arg);
        }

        snprintf(buf, 255, "sudo %s",
                 opt.arg_cmd ? opt.arg_cmd :
                 (tbuf != NULL ? tbuf : arg->command));
        snprintf(buf1, 255, g_tpl_expect_ssh_sudo, buf);

        snprintf(fn, sizeof(fn), tmp_file, arg->host);

        if (!(fp = fopen(fn, "w+"))) {
                fprintf(stderr, "Can't create tmp file for expect : %s!\n",
                        fn);
                return 1;
        }

        len = strlen(buf1);

        if (fwrite(buf1, sizeof(char), len, fp) != len) {
                fprintf(stderr,
                        "Can't create write to tmp file for expect!\n");
                return 1;
        }

        if (chmod(fn, S_IRWXU | S_IRWXG | S_IXOTH) != 0) {
                fprintf(stderr, "Can't chmod tmp file for expect!\n");
                return 2;
        }

        fclose(fp);

        opt.targ_path = "/tmp/";
        opt.src_file = mk_string(opt.src_file, fn, strlen(fn));

        int n;
        if ((n = exec_ssh_cmd(arg, 1)) != 0) {
                printf("*** DEBUG: SCP Failed : %d!\n", n);
                return -1;
        }

        char t[128];

        if (strcmp(arg->script_file, "") != 0) {
                printf(":::IN script_file: %s, %d\n",
                       arg->script_file, (int) strlen(arg->script_file));
                opt.src_file = mk_string(opt.src_file,
                                           arg->script_file,
                                           strlen(arg->script_file));

                printf("*** DEBUG, opt_src_file: %s\n", opt.src_file);
                if (exec_ssh_cmd(arg, 1) != 0) {
                        printf("*** DEBUG: SCP Failed!\n");
                        return -1;
                }

                /*
                snprintf(t, 128, "%s%s", opt.targ_path,
                         ssh_basename(fn));

                opt.targ_path = "";
                opt.src_file = "";
                arg->command = mk_string(arg->command, t, strlen(t));
                printf("%d\n", exec_ssh_cmd(arg, 1));
                return 0;
                */
        }

        free(opt.src_file);
        opt.targ_path = "";
        opt.src_file = "";
        snprintf(t, 128, "%s '%s'", fn, arg->passwd);
        arg->command = mk_string(arg->command, t, strlen(t));
        exec_ssh_cmd(arg, 0);
        unlink(fn);
        return 0;
}


static int create_pool(int pool_size,
                       char *command,
                       char *type,
                       int port,
                       real_ssh_arg *rsarg)
{
        int i = 0;
        pid_t pid;

        real_ssh_arg * tmp, *tmp1;
        tmp = tmp1 = rsarg;

        pool_size = abs(pool_size > MAX_CONCURRENT ? MAX_CONCURRENT : pool_size);

        for (i = 0; i < pool_size; i++) {
                if (strcmp(opt.group, "") != 0 &&
                    strcmp(opt.group, tmp->group) != 0) {
			tmp1 = tmp;
			tmp = tmp1->next;
                        continue;
                }

                tmp1 = tmp;
                msg(MCR_MSG_NORMAL,
                    "Creating task id: %d... host: %s, port: %d, group: %s.\n",
                       i, tmp->host, tmp->port, tmp->group);

                switch (pid = fork()) {
                        case -1:
                                err_exit("The fork failed!");
                                break;

                        case 0:
                                if (strcmp(opt.exec_type, "rssh") == 0) {
                                        if (exec_ssh_cmd(tmp, 1) != SSH_OK) {
                                        }
                                } else if (strcmp(opt.exec_type, "expect") == 0)
                                        exec_expect(tmp);
                                /*
                                console_color("green");
                                printf("*** INFO %d: Done!\n",
                                       i);
                                       */
                                console_color("white");
                                exit(0);
                        default:
                                break;
                }
                tmp = tmp1->next;
        }

        return 0;
}

/*
static int mcr_parse_hosts_fcmd(char * hosts, int *hosts_num, real_ssh_arg *rsarg)
{
        char seps[] = ":";
        char *token;

        *hosts_num = 0;

        if (strstr(hosts, seps) == NULL) {
                (*hosts_num)++;
                rsarg->host = hosts;
        } else {
                token = strtok(hosts, seps);
                while(token != NULL) {
                        if (*hosts_num >= MAX_CONCURRENT)
                                break;
                        rsarg->host = token;
                        token = strtok( NULL, seps );
                }
        }

        return 0;
}
*/

static int process_cmd(char *command,
                       char *type,
                       char *cfg_file,
                       char *hosts,
                       int port,
                       int concurrent)
{
        int hosts_num = 0;
        struct stat sb;

        real_ssh_arg *rsarg = NULL, *tmp, *tmp1;

        if (strlen(hosts) == 0)
                err_exit("Hosts required!\n");

        if (access(hosts, 0) == 0) {
                if (stat(hosts, &sb) == -1)
                        err_exit("Hosts file access failed\n");
                if ((sb.st_mode & S_IRWXG) > 0 || (sb.st_mode & S_IRWXO) > 0)
                        err_exit("Hosts file too open, plase change it to 0600.\n");

                rsarg = (real_ssh_arg*)mcr_parse_hosts_ffile(hosts, &hosts_num);
        } else {
                err_exit("Hosts file access failed\n");
//                mcr_parse_hosts_fcmd(hosts, &hosts_num, rsarg);
        }

        msg(MCR_MSG_NORMAL, "TOTAL HOSTS: %d\n", hosts_num);

        tmp1 = rsarg;

        while (tmp1->next != NULL) {
                tmp = tmp1;
                if (command != NULL)
                        tmp->command = command;
                if (tmp->port == 0)
                        tmp->port = port;
                tmp1 = tmp->next;
        }
        if (command != NULL)
                tmp1->command = command;
        if (tmp1->port == 0)
                tmp1->port = port;

        create_pool(hosts_num, command, type, port, rsarg);

        return 0;
}

static void _signal_proc(int signo)
{
        if (signo == SIGCHLD)
                msg(MCR_MSG_NORMAL,
                    "Thread Done, errno: %d.\n", errno);
        else
                msg(MCR_MSG_NORMAL,
                    "SIGNAL received: signo: %d, signum: %d.\n",
                    signo, __sig_num);
        if (__sig_num++ > 0 && signo == SIGINT) {
                msg(MCR_MSG_WARING, "SIGINT received over 2 times, ");
                ask_exit();
        }
}

static void signal_proc()
{
        if (signal(SIGCHLD, _signal_proc) == SIG_ERR ||
            signal(SIGTERM, _signal_proc) == SIG_ERR ||
            signal(SIGQUIT, _signal_proc) == SIG_ERR ||
            signal(SIGINT, _signal_proc) == SIG_ERR)
                err_exit("Can not process signal!\n");
}

static void add_ssh_keys()
{
        DIR *dp;
        struct dirent *de;
        char fn[255];
        char *nm;

        if (opt.dir_nomatch == NULL)
                nm = "id_rsa";
        else
                nm = opt.dir_nomatch;

        if (access(opt.keys_dir, R_OK | X_OK) != 0)
                return;

        if ((dp = opendir(opt.keys_dir)) == NULL)
                err_exit("Can not open keys dir!\n");
        
        while ((de = readdir(dp))) {
                if (strstr(de->d_name, "pub") != NULL ||
                    strstr(de->d_name, nm) == NULL)
                        continue;
                snprintf(fn, 254, "%s %s/%s", "ssh-add", opt.keys_dir,
                         de->d_name);
                system(fn);
        }
        return;
}


static void thelp()
{
        printf("Distributed ssh executor help:\n"\
               " Usgae ./dmcr -ef <hosts list file(encrypted)> -c command [OPTIONS]\n"\
               " # hosts list file format:\n"\
               " # [GROUP] # group name (optional)\n"\
               " # IP,PORT,COMMAND,USER,PASS,SCRIPT_FILE,PRIVATE_KEY,PRIVATE_PASS\n"\
               " -h\t\t\tdisplay this help and exit \n"\
               " -d\t\t\topen debug mode \n"\
               " -c\t\t\texecute command \n"\
               " -g\t\t\thosts group [GROUP]\n"\
               " -s\t\t\tpush local script adn execute it\n"\
               " -a\t\t\targuments of script\n"\
               " -i\t\t\texecute command from cmd.info file \n"\
               " -t, --type\t\texecute type: cmd, expect\n"\
               " -f, --hosts <file>\thosts list file\n"\
               " -e\t\t\tread an encrypted file\n"\
               " -r <filename>\t\tencrypt file using blowfish method, file saved in\n"\
               " -u <filename>\t\tdecrypt file\n\t\t\tfile.cd\n"\
               " --vim-crypt <1|0>\tusing vim crypt compatible\n"\
               " --cfg-file\t\tcfg template file\n"\
               " --concurrent\t\tmax-concurrent\n"\
               " --targ-path\t\tremote host target path\n"\
               " --src-file\t\tsource file\n"\
               " --targ-owner\t\tremote target file owner\n"\
               " --port\t\t\thost port\n"\
               " --all-msg\t\twaiting for all message from server repsponse, default size is per read\n"\
               " --keys-dir\t\trun ssh-add private keys from --keys-dir\n"
              );
        exit (0);
}

static void handle_crypt()
{
        char *key;

        opt.debug = 2;

        if (opt.decrypt) {
                char *decrypted;
                decrypted = decrypt_file(opt.decrypt, 1);
                printf("%s\n", decrypted);
                return;
        }

        msg(MCR_MSG_NORMAL, "Encrypt file: %s ...\n", opt.crypt_str);
        if (!encrypt_file(opt.crypt_str)) {
                msg(MCR_MSG_ERR_FATAL, "Failed.\n");
                exit(-1);
        }
        msg(MCR_MSG_RESULT, "Done.\n");
}

static char *decrypt_file(const char *file, int print)
{
        return do_crypto(file, 0, print, 0);
}

static char* encrypt_file(char *file)
{
        return do_crypto(file, 1, 0, 0);
}

static void ask_exit()
{
        char buf[4];

        printf("Press yes to exit! ");
        scanf("%3s", buf);
        if (strcmp(buf, "yes") != 0)
                return;
        exit(-1);
}

static real_ssh_arg * mcr_parse_hosts_ffile(char *file, int *hosts_num)
{
        FILE *fp = NULL;
        char s[MAX_LINE_LEN + 1];
        char seps[] = ",";
        char *token;
        int pos = 0;
        char group[64] = "";
        char *decrypted = NULL, *passwd;
        int first = 1;

        real_ssh_arg *h, *pre, *suc;
        *hosts_num = 0;

        if ((h = (real_ssh_arg *) malloc(sizeof(real_ssh_arg))) ==
            NULL)
                err_exit("Could not alloc memory : mcr_parse_hosts_ffile!\n");

        if (opt.hosts_encrypted) {
                decrypted = decrypt_file(file, 0);
                if (validate_decrypted(decrypted) != 0)
                        err_exit("Could not decrypt hosts list file!\n");
        }

        if (!decrypted && (fp = fopen(file, "r")) == NULL)
                err_exit("Could not open hosts file!\n");

        h->next = NULL;
        pre = h;
        memset(s, 0, sizeof(s));
        while ((!decrypted && fgets(s, MAX_LINE_LEN, fp)) ||
                (decrypted && str_getline(decrypted, s, first))) {
                first = 0;
                if (strcmp(s, "") == 0) {
                        memset(s, 0, sizeof(s));
                        break;
                }

                if (s[0] == '#') {
                        memset(s, 0, sizeof(s));
                        continue;
                }

		if (s[0] == '[') {
			memset(group, 0, sizeof(group));
			strncpy(group, strchr(s, '[') + 1, sizeof(pre->group) - 1);
			group[strlen(group) - 1] = '\0';
			if (*hosts_num == 0)
				strncpy(pre->group, group, sizeof(pre->group));
                        memset(s, 0, sizeof(s));
			continue;
		}
		                
                if (strstr(s, seps) == NULL) {
                        if (*hosts_num > 0) {
                                if ((suc =
                                     (real_ssh_arg *) malloc(sizeof(real_ssh_arg))) ==
                                    NULL)
                                        err_exit("Could not alloc memory : mcr_parse_hosts_ffile!\n");
                                pre->next = suc;
                                suc->next = NULL;
                                pre = suc;
                        }
                        pre->host = s;
                        ++*hosts_num;
                } else {
                        pos = 0;

                        token = strtok(s, seps);

                        if (*hosts_num > 0) {
                                if ((suc =
                                     (real_ssh_arg *) malloc(sizeof(real_ssh_arg))) ==
                                    NULL)
                                        err_exit("Could not alloc memory : mcr_parse_hosts_ffile!\n");
                                pre->next = suc;
                                suc->next = NULL;
                                pre = suc;
				strncpy(pre->group, group, sizeof(pre->group));
                        }

                        pre->private_key = NULL;
                        pre->private_pass = NULL;
                        while (token != NULL) {
                                if (*hosts_num >= MAX_CONCURRENT)
                                        break;
                                if (pos == 0) {
                                        if (validate_char(token, MAX(5,strlen(token))) != 0)
                                                goto skip;
                                        pre->host = mk_string(pre->host,
                                                              token,
                                                              strlen(token));
                                } else if (pos == 1) {
                                        pre->port = atoi(token);
                                } else if (pos == 2) {
                                        pre->command = mk_string(pre->command,
                                                                 token,
                                                                 strlen(token));
                                } else if (pos == 3) {
                                        pre->user = mk_string(pre->user,
                                                              token,
                                                              strlen(token));
                                } else if (pos == 4) {
                                        pre->passwd = mk_string(pre->passwd,
                                                               token,
                                                               strlen(token));
                                } else if (pos == 5) {
                                        pre->script_file = mk_string(pre->script_file,
                                                                     token,
                                                                     strlen(token));
                                } else if (pos == 6) {
                                        pre->private_key = mk_string(pre->private_key,
                                                                     token,
                                                                     strlen(token));
                                } else if (pos == 7) {
                                        pre->private_pass = mk_string(pre->private_pass,
                                                                     token,
                                                                     strlen(token));
                                }

                                ++pos;
                                token = strtok(NULL, seps);
                        }

                        ++*hosts_num;
                }

                memset(s, 0, sizeof(s));
skip:
                continue;
        }

        if (fp)
                fclose(fp);
        return h;
}

static char* parse_cmd(const char *cmd, int field)
{
        FILE *fp;
        char buf[255];

        if (!(fp = fopen(CMD_FILE_INFO, "r")))
                return NULL;

        while (fgets(buf, sizeof(buf), fp) != NULL) {
                if (strstr(buf, cmd) != NULL) {
                        fclose(fp);
                        return split(str_cut_enter(buf), "=", field);
                }
        }
        fclose(fp);
        return NULL;
}

static int validate_decrypted(const char *decrypted)
{
        int len = strlen(decrypted);
        if (len < 24)
                return 1;

        if (decrypted[0] < 10 || decrypted[0] > 126 ||
            decrypted[1] < 10 || decrypted[1] > 126 ||
            decrypted[2] < 10 || decrypted[2] > 126 ||
            decrypted[3] < 10 || decrypted[3] > 126 ||
            decrypted[4] < 10 || decrypted[4] > 126 ||
            decrypted[len - 1] < 10 || decrypted[len - 1] > 126 ||
            decrypted[len - 2] < 10 || decrypted[len - 2] > 126 ||
            decrypted[len - 3] < 10 || decrypted[len - 3] > 126 ||
            decrypted[len / 2] < 10 || decrypted[len / 2] > 126)
                return 2;

        return 0;
}

/*
static void cfmakeraw(struct termios *termios_p)
{
        termios_p->c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON);
        termios_p->c_oflag &= ~OPOST;
        termios_p->c_lflag &= ~(ECHO|ECHONL|ICANON|ISIG|IEXTEN);
        termios_p->c_cflag &= ~(CSIZE|PARENB);
        termios_p->c_cflag |= CS8;
}
*/
static int ointeractive_shell_session(ssh_session session, ssh_channel channel)
{
        int rc = 0;
        /* Session and terminal initialization skipped */
        char buffer[256];
        int nbytes, nwritten;
        while (ssh_channel_is_open(channel) &&
               !ssh_channel_is_eof(channel))
        {
                struct timeval timeout;
                ssh_channel in_channels[2], out_channels[2];
                fd_set fds;
                int maxfd;
                timeout.tv_sec = 30;
                timeout.tv_usec = 0;
                in_channels[0] = channel;
                in_channels[1] = NULL;
                FD_ZERO(&fds);
                FD_SET(0, &fds);
                FD_SET(ssh_get_fd(session), &fds);
                maxfd = ssh_get_fd(session) + 1;
                ssh_select(in_channels, out_channels, maxfd, &fds, &timeout);
                if (out_channels[0] != NULL)
                {
                        nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
                        if (nbytes < 0) return SSH_ERROR;
                        if (nbytes > 0)
                        {
                                nwritten = write(1, buffer, nbytes);
                                if (nwritten != nbytes) return SSH_ERROR;
                        }
                }
                if (FD_ISSET(0, &fds))
                {
                        nbytes = read(0, buffer, sizeof(buffer));
                        if (nbytes < 0) return SSH_ERROR;
                        if (nbytes > 0)
                        {
                                nwritten = ssh_channel_write(channel, buffer, nbytes);
                                if (nbytes != nwritten) return SSH_ERROR;
                        }
                }
        }
        return rc;
}

/* Under Linux, this function determines whether a key has been pressed.
   Under Windows, it is a standard function, so you need not redefine it.
   */
int kbhit()
{
        struct timeval tv = { 0L, 0L };
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(0, &fds);
        return select(1, &fds, NULL, NULL, &tv);
}
/* A very simple terminal emulator:
   - print data received from the remote computer
   - send keyboard input to the remote computer
   */
int interactive_shell_session(ssh_channel channel)
{
        int rc = 0;
        /* Session and terminal initialization skipped */
        char buffer[256];
        int nbytes, nwritten;
        while (ssh_channel_is_open(channel) &&
               !ssh_channel_is_eof(channel))
        {
                nbytes = ssh_channel_read_nonblocking(channel, buffer, sizeof(buffer), 0);
                if (nbytes < 0) return SSH_ERROR;
                if (nbytes > 0)
                {
                        nwritten = write(1, buffer, nbytes);
                        if (nwritten != nbytes) return SSH_ERROR;
                        if (!kbhit())
                        {
                                usleep(50000L); // 0.05 second
                                continue;
                        }
                        nbytes = read(0, buffer, sizeof(buffer));
                        if (nbytes < 0) return SSH_ERROR;
                        if (nbytes > 0)
                        {
                                nwritten = ssh_channel_write(channel, buffer, nbytes);
                                if (nwritten != nbytes) return SSH_ERROR;
                        }
                }
                return rc;
        }
}

static int validate_char(const char *str, int len)
{
        int tl, i;

        tl = strlen(str);
        if (tl < len) {
                fprintf(stderr, "Validate char failed, size!\n");
                return 1;
        }

        for (i = 0; i < len; i++)
                if (str[i] < 10 || str[i] > 126) {
                        fprintf(stderr, "Validate char failed!\n");
                        return 1;
                }

        return 0;
}

static void init_options()
{
        opt.crypt_str = NULL;
        opt.arg = NULL;
        opt.internal_cmd = NULL;
        opt.targ_path = NULL;
        opt.arg_cmd = NULL;
        opt.src_file = NULL;
        opt.targ_owner = NULL;
        opt.targ_mode = NULL;
        opt.group = NULL;
        opt.exec_type = "rssh";
        opt.keys_dir = NULL;
        opt.decrypt = NULL;
        opt.dir_nomatch = NULL;
        opt.debug = 0;
        opt.vim_crypt = 0;
        opt.hosts_encrypted = 0;
        opt.allmsg = 0;
        opt.exec_script = 0;
}
