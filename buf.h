#ifndef BUF_INCLUDED
#define BUF_INCLUDED

/* Create string use malloc */
char* mk_string(char *s, const char *str, size_t size);
/* Split string: str with sep */
char* split(char *str, char* sep, int field);
/* Remove br from string: str */
char * str_cut_enter(char *str);
/* Get line from string: str */
int str_getline(char *str, char res[], int first);
int read_file(const char *file, char **fbuf);

#endif

